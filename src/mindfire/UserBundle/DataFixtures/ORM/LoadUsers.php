<?php

namespace mindfire\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use mindfire\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadUsers implements FixtureInterface, ContainerAwareInterface
{
    /*
     * @var ContainerInterface
     */

    private $container;

    /*
     * {@inheritDoc}
     */

    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setUsername('varun');
        $user->setPassword($this->encodePassword($user, 'varunpass'));
        $manager->persist($user);

        $admin = new User();
        $admin->setUsername('stark');
        $admin->setPassword($this->encodePassword($admin, 'starkpass'));
        $admin->setRoles(array('ROLE_ADMIN'));
        $manager->persist($admin);


        $manager->flush();
    }

    private function encodePassword(User $user, $plainPassword)
    {
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        return $encoder->encodePassword($plainPassword, $user->getSalt());
    }


    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
        $this->container = $container;
    }
}